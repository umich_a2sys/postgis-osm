#!/usr/bin/env python
"""
This CLI program will allow a user to download necessary dependencies and interact with 
the docker container
"""
import os
import tarfile

import wget

CURRENT_DIR = os.path.dirname(os.path.realpath(
    __file__))   # the directory of this file
# the direcotry to store imposm
IMPOSM3_DIR = os.path.join(CURRENT_DIR, 'imposm3')
# the directory to store osmconvert
OSMCONVERT_DIR = os.path.join(CURRENT_DIR, 'osmconvert')

DEFAULT_OS = 'LINUX' # default OS

IMPOSM3_URL = {
    'LINUX': 'https://imposm.org/static/rel/imposm3-0.4.0dev-20170519-3f00374-linux-x86-64.tar.gz'
}
IMPOSM3_FULL_NAME = os.path.join(CURRENT_DIR, 'imposm3-0.4.0dev-20170519-3f00374-linux-x86-64')

OSMCONVERT_URL = {
    'LINUX': 'http://m.m.i24.cc/osmconvert64'
}


def extract_tar(fname, folder):
    with tarfile.open(fname) as tar:
        tar.extractall(path=folder)


def make_folder(folder):
    if not os.path.exists(folder):
        os.makedirs(folder)


def get_impoms3():
    print("Getting imposm3")
    filename = wget.download(IMPOSM3_URL[DEFAULT_OS], CURRENT_DIR)
    extract_tar(filename, CURRENT_DIR)
    os.rename(IMPOSM3_FULL_NAME, IMPOSM3_DIR)
    os.remove(filename)


def get_osmconvert():
    """ Gets OSM convert binary """
    print("Getting osmconvert")
    make_folder(OSMCONVERT_DIR)
    wget.download(OSMCONVERT_URL[DEFAULT_OS], OSMCONVERT_DIR)


def dependencies():
    """Gathers binary dependencies"""
    print("Gathering the dependencies for project")
    if not os.path.exists(IMPOSM3_DIR):
        get_impoms3()
    if not os.path.exists(OSMCONVERT_DIR):
        get_osmconvert()

def main():
    dependencies()

if __name__ == '__main__':
    main()
