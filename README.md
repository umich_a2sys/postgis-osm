# POSTGIS-OSM - Docker Image

This repository holds the docker image to help with importing OSM data into a PostGIS database. It contains everything you need, including
[Imposm3](https://imposm.org/docs/imposm3/latest/index.html). It also includes instruction on how to convert the PostGIS database to Spatialite.

## Short Docker Intro / Install Instructions
Docker is a `container` creation and management system.  Many people are familiar with Virtual Machines (VMS), however a container provides a much lighter and thinner abstraction. A container shares the host operating system (if running on Linux), but still provides good separation. See this [article](https://stackoverflow.com/questions/16047306/how-is-docker-different-from-a-normal-virtual-machine) for more info.

The tools (and their dependencies) to import OSM data into databases can be daunting; but with Docker we can make this much easier. All we need to do is run our docker container and all tools we need are ready! This includes:

* PostGIS
* Python
* PgAdmin
* Imposm
* And all their dependencies!

Install `docker` by following these [instructions](https://docs.docker.com/engine/installation/linux/docker-ce/ubuntu/).

The `Dockerfile` in the folder `docker` defines the *build steps* to construct a docker image. You will not need to build the image however, because a built image is already in the cloud, you simply run `docker pull jeremybyu/postgis-osm`.

### Installing Local Dependencies
If you are on linux and desire to install imposm3 locally you *may* do that as well. This is just a small binary file that actually does the importing.  The following script will download the imposm3 dependency for you and store it in the imposm3 folder.

## Setup POSTGIS Docker Container

1. `cd postgis-docker` # Ensure we are in the git repository
1. `docker pull jeremybyu/postgis-osm`
2. `docker run --name osm -d -p 5050:5050 -p 5432:5432 -v ${PWD}:/home/postgis-osm jeremybyu/postgis-osm`

The second command pulls down the docker **image** from the docker registry. The third command does the following:

* instantiates the image as a **container** (named `osm`)
* starts the container as a **d**aemon (runs in background)
* maps the containers **p**orts (5050, 5432) to your localhost ports.
* mounts your current working directory (pwd) as a **v**olume accessible to the container (as `/home/postgis-osm`)

The container is now running.  You can see it by running `docker ps`.  If you desire to kill the container (which will off course stop the database server, but NOT delete the data) please run `docker kill osm`. If you want to start up the container again just type `docker start osm`.

To completely remove the container (and associated data) run `docker rm osm`. If you want to create the container again you must start with step 3.

## Importing OSM
We will use the application called [imposm](https://imposm.org/docs/imposm3/latest/) to import large OSM files (.pbf, binary format of .osm) into our POSTGIS database. It works in *two* stages. First it reads the OSM file by using a `mapping.json` (example provided in `imposm/mapping.json`)
to understand the structure of your desired database tables; the results are saved in a cache. Second it **writes** to the databse using the first stage generated cache. For more details on imposm3 please read the documentation.

For convenience the docker container comes with this application installed, but you are free to download and use the binary yourself. The following commands are for using `imposm3` inside the docker container, but they can be slightly changed if you downloaded imposm3 yourself.

1. `docker exec -i -t osm bash -c '${IMPOSM_BIN} import -config config.json -read new-york-latest.osm.pbf -overwritecache'`
2. `docker exec -i -t osm bash -c '${IMPOSM_BIN} import -config config.json -write'`

The first command and second command execute the imposm binary (`${IMPOSM_BIN}`).  The first command **reads** in the OSM `.pbf` file into a cache.  The second command uses that cache to **write** into the database.


### Importing OSM data using imposm3 that is installed locally

If you have imposm3 locally installed then you can modify the above instructions as follows

1. `./imposom3/imposm3 import -config config.json -read new-york-latest.osm.pbf -overwritecache`
2. `./imposom3/imposm3 import -config config.json -write`


Once you have finished importing you should be able to view your database and see your new tables.  For example, the provided `mapping.json` ends up looking like this when viewed with [PgAdmin](#view-database-with-pgadmin).

![Example](pics/pgadmin-example.png "Example")

### Finding PBF Files

Here is a great resource for finding region specific OSM files in the PFB format. [Link](https://download.geofabrik.de/)

## View Database with PgAdmin
Sometimes its nice to view the database, where you can actually see what has been imported.  You can use `pgadmin` for this, which is already installed in the container  It works by launching a webserver in the container, which is then mapped to your localhost at port 5050. After the container is already running please run the following command.

1. `docker exec -i -t osm bash -c 'python $PGADMIN/pgAdmin4.py'`
2. Navigate to localhost:5050

The following only needs to be done once:

1. Right Click Server. Select Create -> Server
2. Set Name to `postgis`
3. Change to connection tab. Fill out settings as shown in picture. Passwword is `password`.

You should be good to go now! If you ever need to reset something, be sure to clear you cache with a hard reset.

![New Server](pics/pgadmin.png "Credentials")

In the terminal, you can close pgadmin with `ctrl-c` anytime, and `pgadmin` will shut down. 

## Convert PostGIS to Spatialite
Its really nice to have a small fast, embeddable, spatially aware database like [spatialite](http://www.gaia-gis.it/gaia-sins/). Luckily there is a tool where we can convert our PostGIS database to a small spatialite database; it's called `og2ogr`. We will install this tool **locally** on your computer. Install it with the following:

1. `sudo add-apt-repository ppa:ubuntugis/ppa && sudo apt-get update` # only needed if you dont have this PPA yet...
2. `sudo apt-get install gdal-bin`
3. `ogr2ogr -f SQLite -dsco SPATIALITE=yes DB_FILE_NAME.db PG:"dbname=osm user=osm password=osm host='localhost' port='5432'" import.osm_buildings`

The first and second command setup and install `gdal-bin` package which comes with `ogr2ogr`.  The third command tells ogr2ogr to read from our PostGIS database and output to a spatialite database. The very last argument specifies the table we want, in this case import.osm_buildings. 


## Interact
If you simply want to have a bash session to interact with the container, please execute the following lines. This should be run after you have started the container. 

1. `docker exec -i -t osm /bin/bash`

